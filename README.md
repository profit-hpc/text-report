# Re-implementation of ASCII-Report

## Goals

- Providing a solid specification for the data used in the reports to ensure their compatibility.
- Providing a tool for the data verification according to the spec.
- Implementation of a clean interface between data collection/storage and report generation.
- Ensuring an industry-level code quality, documentation and test coverage.

## Current status (October 2018)

| Category | Task | Planned | WIP | Finished | Additional info |
| --- | --- | --- | --- | --- | --- |
| Spec | Minimal data set | | | ![X](docs/marker.png) | [link](docs/spec-metrics.md) |
| Spec | Extended data set | | | ![X](docs/marker.png) | [link](docs/spec-metrics2.md) Should also be valid for PDF-report and Grafana |
| Impl | Text report | | | ![X](docs/marker.png) | |
| Impl | Spec parser | | ![X](docs/marker.png) | | |
| Impl | automatic schema verification | | ![X](docs/marker.png) | | |
| Impl | automatic test generation | ![X](docs/marker.png) | | | |
| Doc | automatic API documentation | | | ![X](docs/marker.png) | [link](docs/api-doc.md) |
| QA | Continuous testing pipeline | | ![X](docs/marker.png) | | |
| QA | Uniform code style | | | ![X](docs/marker.png) | [![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black) |
| QA | Consistent error handling | | ![X](docs/marker.png) | | |
| QA | Automatic linting and checking | | | ![X](docs/marker.png) | ![linter](https://gibraltar.chi.uni-hannover.de/profit-hpc/text-report/raw/master/docs/linter.svg) |
| QA | Basic unit testing | | ![X](docs/marker.png) | | ![tests](https://gibraltar.chi.uni-hannover.de/profit-hpc/text-report/raw/master/docs/tests.svg) |
| QA | Industry-level test coverage | | ![X](docs/marker.png) | | ![coverage](https://gibraltar.chi.uni-hannover.de/profit-hpc/text-report/badges/master/coverage.svg) |

## Requirements

- python3
- python3-pip

## Installation and Report generation

First, the repository has to be cloned to local machine:

```bash
git clone git@gibraltar.chi.uni-hannover.de:profit-hpc/text-report.git .
cd text-report
```

Then, Python dependencies need to be installed:

```bash
python3 -m pip install -r requirements.txt
```

To generate the report, use the one last step (use the proper name instead of `data.json`):

```bash
python3 runme.py data.json
```

The script checks if the data is complete and valid (you'll see a message about validity on the screen), and if it's the case, the report is written to the `out` folder and can be viewed by the common UNIX means (cat or less).

## Project structure

- [conf] - default configuration files/templates
  - config.py
- [docs] - project documentation and specifications
- [errors] - base classes for all exceptions used in the project
- [format] - output formats for the report
  - text.py
  - latex.py
  - pdf.py
- [testdata] - snapshot of data for testing or demonstrating features
  - stats.csv
  - minset-valid.json - a JSON file according to spec for the input data + minimal metrics set
  - minset-relaxed.json - valid metrics file, if unspecified values are allowed
  - minset-invalid.json - an example of an invalid metrics file
  [spectools] - tools for extraction of data schema from the spec for validation and test generation
- [tools] - common utils used in other modules
- [validator] - current implementation of the data validator
- runme.py - expects JSON file as parameter and generates a text report in the "out" folder
- test_all.py - runs tests
- README.md - this file
- requirements.txt - necessary python modules
- .gitignore

## Development

We're using a couple of tools to ensure standard-compliance and PEP-compliant formatting.

- Code is formatted using Black [![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black), with a line length set to 79 (as proposed by the standard).
- Every commit is statically checked/linted using [prospector](https://github.com/PyCQA/prospector/) with strictness set to "veryhigh", and all warnings enabled. As soon as the code reaches some maturity, non-complying commits will be rejected.
- For test coverage, [pytest](https://github.com/pytest-dev/pytest/) with [coverage.py](https://github.com/nedbat/coveragepy/) is used - in the future, as soon as we get a total coverage of over 90%, new commits with test coverage below that will be rejected.
