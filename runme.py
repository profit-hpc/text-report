#! /usr/bin/env python3
"""Script to validate input data and create a report."""

import sys
import argparse
import json

from tools.files import save_str
from tools.files import load_json
from format.text import TextReport
from validator.validator import Validator


# TODO: pass spec version as parameter (1.0, demo, 2.0)

def main():
    parser = argparse.ArgumentParser(description="""
        Gets the job information in JSON format and
        outputs the text report.
    """, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-o", "--output", nargs='?', help="output file")
    parser.add_argument("INPUT", nargs='?', help="input JSON file")

    args = parser.parse_args()

    JNAME = args.INPUT if args.INPUT is not None else False
    RNAME = args.output if args.output is not None else False

    if JNAME is False:
        if not sys.stdin.isatty():
            DATA = json.load(sys.stdin)
        else:
            print("No input provided")
            exit(1)
    elif JNAME is not False:
        DATA = load_json(JNAME)


    VAL = Validator(DATA)
    if not VAL.is_valid():
        print("Job data has errors: {}".format(VAL.verbose()))
        print("Report generation failed.")
        sys.exit(1)


    REP = TextReport(DATA)
    if RNAME == False:
        print(REP.report())
    else:
        save_str(RNAME, REP.report())
        print("report written to {}".format(RNAME))


if __name__ == "__main__":
    main()
