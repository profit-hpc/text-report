#! /usr/bin/env python3
"""WIP: Testing plot creation in python."""


import matplotlib.pyplot as plt


def make_diag(data):
    """
    WIP: function for building diagrams.

    Arguments
    ---------
        data -- pandas table

    Returns
    -------
        matplotlib figure

    """
    x_vals = data.timestamp
    y_vals = data.load_1

    fig, axes = plt.subplots()
    axes.plot(x_vals, y_vals)

    axes.set(ylabel="CPU load", xlabel="time", title="testplot")
    axes.grid()

    return fig
