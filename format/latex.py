#! /usr/bin/env python3
"""WIP: testing LaTeX report creation in python."""

from pylatex import Document
from pylatex import Section
from pylatex import Command
from pylatex import Figure
from pylatex.utils import NoEscape

from conf.config import COLS


def make_tex(title):
    """
    WIP: function to build the LaTeX document.

    Arguments
    ---------
        string title -- title of the document

    """
    doc = Document()

    doc.preamble.append(Command("title", title))
    doc.preamble.append(Command("author", "Anonymous author"))
    doc.preamble.append(Command("date", NoEscape(r"\today")))
    doc.append(NoEscape(r"\maketitle"))

    with doc.create(Section("General information")):
        doc.append("It was a very bad job ... ")
        doc.append("You wasted a huge amount of resources ... ")

    with doc.create(Section("Plots")):
        for i in COLS:
            with doc.create(Figure(position="h!")) as pic:
                pic.add_image("{}.png".format(i), width="400px")
                pic.add_caption("Visualization of {}".format(i))

    # doc.generate_tex()
    # doc.generate_pdf(title, clean_tex=False)
    # doc.dumps()
    return doc
