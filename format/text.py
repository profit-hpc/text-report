#! /usr/bin/env python3
"""Text report generator module."""

import textwrap
import math
import re

from datetime import datetime

from conf.config import CLEARNAME_MAPPING as CM
from conf.config import NAME_COL
from conf.config import STRLEN
from conf.config import VALUE_COL
from errors.error import DataFormatError
from tools.strings import *
from validator.validator import Validator

TIMECONV = datetime.fromtimestamp


def sep(symbol="-", length=STRLEN):
    """
    Build a separator line.

    Keyword Arguments
    -----------------
        character symbol -- character to use as a separator
        integer length -- length of the line

    Returns
    -------
        separator string wrapped as array

    """
    return [symbol * length]


def center_underline(text):
    """
    Create a centered line and a separator line.

    Arguments
    ---------
        string text -- text to format

    Returns
    -------
        formatted text and separator as string array

    """
    return center(text, symbol=" ") + sep()


def header(title="ProfiT-HPC Report"):
    """
    Create a report header.

    Keyword Arguments
    -----------------
        string title -- Title of the report, default: ProfiT-HPC Report

    Returns
    -------
        formatted title string

    """
    return sep()\
        + center(title)\
        + center(datetime.now().strftime("%d %b %Y %H:%M:%S"))\
        + sep()



def subheader(title):
    """
    Create a second-level header for the report.

    Arguments
    ---------
        string title -- title of the subheader

    Returns
    -------
        formatted title

    """
    return [""] + center_underline(title)


def subsubheader(title):
    """
    Create a third-level header for the report.

    Arguments
    ---------
        string title -- title of the subsubheader

    Returns
    -------
        formatted title

    """
    return [""] + center("** " + title + " **")


def kv2str(key, val, left=NAME_COL, right=VALUE_COL):
    """Format a key-value pair."""
    return "{1:{0}}{3:{2}}".format(left, key + ": ", right, str(val))

def kv2str_wrap(key, val, left=NAME_COL, right=VALUE_COL):
    """Format a key-value pair."""
    lines = textwrap.wrap(str(val), right)
    output = "{1:{0}}{3:{2}}".format(left, key + ": ", right, lines[0])

    if len(lines) > 1:
        output += "\n".join("{1:{0}}{3:{2}}".format(left, " ", right, s) for s in lines[1:])

    return output


def warn2str(text, length=STRLEN):
    """Format a warning."""
    strs = textwrap.wrap("{}".format(text, length - 2))

    for i in range(0, len(strs)):
        if i == 0:
            strs[i] = "- " + strs[i]
        else:
            strs[i] = "  " + strs[i]

    return strs


def get_entry(key=None, val=None, wrap=False):
    """
    Convert a key-value pair to a formatted string, resolves key.

    Keyword Arguments
    -----------------
        string key -- left side of the string
        string val -- right side of the string
        bool wrap  -- wrap value to multiple lines

    Returns
    -------
        formatted string

    """
    if key is None:
        return [""]
    if val is None:
        return [kv2str(CM[key], "n/a")]

    if wrap:
        return [kv2str_wrap(CM[key], val)]
    else:
        return [kv2str(CM[key], val)]

def get_value(dict, key, format_function=None, **kwargs):
    if key in dict:
        val = dict[key]
        if format_function is None:
            out = val
        else:
            out = format_function(val, **kwargs)
    else:
        out = None

    if out is None:
        out = "n/a"

    return out

def get_value_num_nonone(dict, key):
    if key in dict:
        val = dict[key]
        out = val
    else:
        out = None

    if out is None:
        out = 0

    return out

def spacer():
    """Return an empty line."""
    return get_entry()


def nodes_part(nodes):
    """
    determine node name prefix and node number
    find leading characters and ending numbers
    """
    pattern = re.compile(r"[0-9]+$")

    dictnode={}

    for node in nodes:
        p = pattern.search(node)

        if p is not None:
            span = p.span()

            nodepre = node[:span[0]]
            nodenum = node[span[0]:]

            if nodepre in dictnode:
                dictnode[nodepre].append(nodenum)
            else:
                dictnode[nodepre] = [nodenum]

        else:
            dictnode[node] = ['']

    return dictnode

def numbers_compact(numbers):
    if len(numbers) > 1:
        numbers.sort()
        numlist = str(numbers[0])
        numlast = int(numbers[0])
        cont=''

        for n in range(1, len(numbers)):
            num = numbers[n]
            if (int(num) == numlast + 1):
                cont = '-' + num
            else:
                numlist += cont + ',' + num
                cont = ''
            numlast = int(num)
        numlist += cont
    else:
        numlist = str(numbers[0])

    return numlist

def nodes_compact(nodes):
    if len(nodes) <= 1:
        return nodes[0]

    dictnode = nodes_part(nodes)

    compact_list = []

    for key in dictnode:
        if dictnode[key] != ['']:
            numlist = numbers_compact(dictnode[key])
            if len(dictnode[key]) == 1:
                compact_list.append(key+numlist)
            else:
                compact_list.append(key + '[' + numlist + ']')
        else:
            compact_list.append(key)

    return ", ".join(compact_list)

def calc_cpu_perc(utime, elapsed, cus):
    percentage = ((100 * utime) / cus) / elapsed

    return round(percentage)

class TextReport:
    """Main class for the text report."""

    def __init__(self, data):
        """Instantiate TextReport."""
        self._val = Validator(data=data)
        if not self._val.is_valid():
            raise DataFormatError(
                "Data doesn't conform to the specification ..."
            )
        self._datefmt = "%a %d %b %Y %H:%M:%S"
        self._report = header()

        # sort nodes by hostname
        data["nodes"] = sorted(data["nodes"], key=lambda x: x["node_name"])

        self._gen_general_info(data)
        self._gen_cpu_mem_usage(data)
        self._gen_gpu_info(data["nodes"])
        self._gen_io_network(data)
        self._gen_node_info(data["nodes"])
        self._gen_recommendations(data)

    def _gen_general_info(self, data):
        """Generate global data."""
        self._report += subheader("General Information")
        gen = data["general"]
        self._report += get_entry("user_name", gen["user_name"])
        self._report += get_entry("job_id", data["job_id"])
        self._report += get_entry("used_queue", gen["used_queue"])
        for i in ["num_nodes", "requested_cu"]:
            self._report += get_entry(i, gen[i])

        for i in ["requested_time", "used_time"]:
            self._report += get_entry(i, sec2str(gen[i]))

        if not "exit_code" in gen:
            gen["exit_code"] = -1
        self._report += get_entry("exit_code", ecode2str(gen["exit_code"]))

        for i in ["submit_time", "start_time", "end_time"]:
            self._report += get_entry(
                i, TIMECONV(gen[i]).strftime(self._datefmt)
            )

    def _gen_cpu_mem_usage(self, data):
        """Build per node data of CPU and Memory usage."""
        nodes = data["nodes"]
        self._report += subheader("CPU utilization. Mean values + high water mark (hwm) values")
        columns = 7
        column_len = math.floor(STRLEN / columns) - 1
        first_column_len = STRLEN - (column_len + 1)*(columns - 1) - 1

        # HEADERS
        self._report += ["{h1:{w1}}|{h2:^{w2}}|{h3:^{w3}}|{h4:^{w4}}|".format(
            h1 = " ", w1 = first_column_len,
            h2 = "CPU usage", w2 = column_len * 3 + 2,
            h3 = "memory used", w3 = column_len * 2 + 1,
            h4 = "memory", w4 = column_len,
        )]

        # HEADERS second row
        self._report += ["{h1:^{w1}}|{h2:^{w}}|{h3:^{w}}|{h4:^{w}}|{h5:^{w}}|{h6:^{w}}|{h7:^{w}}|".format(
            w1 = first_column_len, w = column_len,
            h1 = "nodes",
            h2 = "cores",
            h3 = "total[%]",
            h4 = "hwm[%]",
            h5 = "mean[GiB]",
            h6 = "hwm[GiB]",
            h7 = "alloc[GiB]",
        )]

        # Separator
        self._report += [
            "{}+".format("-" * first_column_len) +
            "{}+".format("-" * column_len) * (columns - 1)
        ]

        sums = {
            "num_cores": 0,
            "cpu_times": 0,
            "cpu_usage": 0,
            "mem_rss_avg": 0,
            "mem_rss_max": 0,
            "mem_total": 0,
        }

        for node in nodes:
            sums["num_cores"] += node["num_cores"]
            sums["cpu_times"] += node["cpu_time_user"] + node["cpu_time_system"]
            sums["cpu_usage"] += node["cpu_usage"]
            sums["mem_rss_avg"] += node["mem_rss_avg"]
            sums["mem_rss_max"] += node["mem_rss_max"]

            if "alloc_mem" in node:
                mem_total = node["alloc_mem"] * 1000000
            else:
                cpus = node["sockets"] * node["cores_per_socket"] * (node["virt_threads_per_core"] + 1)
                mem_total = node["num_cores"] * node["available_main_mem"] / cpus

            sums["mem_total"] += mem_total

            self._report += ["{h1:>{w1}}|{h2:>{w}}|{h3:>{w}}|{h4:>{w}}|{h5:>{w}}|{h6:>{w}}|{h7:>{w}}|".format(
                w1 = first_column_len, w = column_len,
                h1 = node["node_name"],
                h2 = node["num_cores"],
                h3 = calc_cpu_perc(node["cpu_time_user"] + node["cpu_time_system"], data["general"]["used_time"], node["num_cores"]),
                h4 = round(int(node["cpu_usage"]) / int(node["num_cores"])),
                h5 = bytes2gib(node["mem_rss_avg"]),
                h6 = bytes2gib(node["mem_rss_max"]),
                h7 = bytes2gib(mem_total),
            )]

        if len(nodes) > 1:
            # Separator
            self._report += [
                "{}+".format("-" * first_column_len) +
                "{}+".format("-" * column_len) * (columns - 1)
            ]

            # Summary
            self._report += ["{h1:>{w1}}|{h2:>{w}}|{h3:>{w}}|{h4:>{w}}|{h5:>{w}}|{h6:>{w}}|{h7:>{w}}|".format(
                w1 = first_column_len, w = column_len,
                h1 = "total",
                h2 = sums["num_cores"],
                h3 = calc_cpu_perc(sums["cpu_times"], data["general"]["used_time"], sums["num_cores"]),
                h4 = round(int(sums["cpu_usage"]) / int(sums["num_cores"])),
                h5 = bytes2gib(sums["mem_rss_avg"]),
                h6 = bytes2gib(sums["mem_rss_max"]),
                h7 = round(float(bytes2gib(sums["mem_total"]))),
            )]

    def _gen_gpu_info(self, nodes):
        """Build per node data of GPU usage."""

        gpu_exists = False

        for node in nodes:
            if "gpus" in node:
                if node["gpus"] is not None:
                    if len(node["gpus"]) > 0:
                        gpu_exists = True

        if gpu_exists is False:
            return

        self._report += subheader("GPU utilization. Mean values + high water mark (hwm) values")
        columns = 7
        column_len = math.floor(STRLEN / columns) - 1
        first_column_len = STRLEN - (column_len + 1)*(columns - 1) - 1

        # HEADERS
        self._report += ["{h1:{w1}}|{h2:^{w2}}|{h3:^{w3}}|".format(
            h1 = " ", w1 = first_column_len,
            h2 = "GPU", w2 = column_len * 4 + 3,
            h3 = "CPU", w3 = column_len * 2 + 1,
        )]

        # HEADERS
        self._report += ["{h1:^{w1}}|{h2:^{w}}|{h3:^{w}}|{h4:^{w}}|{h5:^{w}}|{h6:^{w}}|{h7:^{w}}|".format(
            w1 = first_column_len, w = column_len,
            h1 = "nodes",
            h2 = "ID",
            h3 = "usage",
            h4 = "usage",
            h5 = "memory",
            h6 = "# proc",
            h7 = "usage",
        )]

        # HEADERS second row
        self._report += ["{h1:^{w1}}|{h2:^{w}}|{h3:^{w}}|{h4:^{w}}|{h5:^{w}}|{h6:^{w}}|{h7:^{w}}|".format(
            w1 = first_column_len, w = column_len,
            h1 = "",
            h2 = "[bus]",
            h3 = "mean[%]",
            h4 = "hwm[%]",
            h5 = "hwm[%]",
            h6 = "total",
            h7 = "hwm[%]",
        )]

        # Separator
        self._report += [
            "{}+".format("-" * first_column_len) +
            "{}+".format("-" * column_len) * (columns - 1)
        ]

        sums = {
            "gpus": 0,
            "usage_avg": 0,
            "usage_max": 0,
            "mem_max": 0,
            "mem": 0,
            "cpu_proc_total": 0,
            "cpu_usage_max": 0,
            "num_cores": 0,
        }
        for node_id in range(0, len(nodes)):
            node = nodes[node_id]
            first_gpu = True
            for gpu in node["gpus"]:
                sums["gpus"] += 1
                sums["usage_avg"] += gpu["usage_avg"]
                sums["usage_max"] = max(gpu["usage_max"], sums["usage_max"])
                sums["mem_max"] += gpu["mem_max"]
                sums["mem"] += gpu["mem"]
                sums["cpu_proc_total"] += gpu["cpu_proc_total"]
                sums["cpu_usage_max"] += gpu["cpu_usage_max"]
                sums["num_cores"] += node["num_cores"]

                self._report += ["{h1:>{w1}}|{h2:>{w}}|{h3:>{w}}|{h4:>{w}}|{h5:>{w}}|{h6:>{w}}|{h7:>{w}}|".format(
                    w1 = first_column_len, w = column_len,
                    h1 = node["node_name"] if first_gpu else "",
                    h2 = gpu["bus"],
                    h3 = round(gpu["usage_avg"]),
                    h4 = round(gpu["usage_max"]),
                    h5 = round(gpu["mem_max"]*100/gpu["mem"]),
                    h6 = gpu["cpu_proc_total"],
                    h7 = round(int(gpu["cpu_usage_max"]) / int(node["num_cores"])),
                )]
                first_gpu = False

            # Separator
            if node_id < len(nodes) - 1:
                self._report += [
                    "{}+".format("-" * first_column_len) +
                    "{}+".format("-" * column_len) * (columns - 1)
                ]

        if len(nodes) > 1:
            # Separator
            self._report += [
                "{}+".format("-" * first_column_len) +
                "{}+".format("-" * column_len) * (columns - 1)
            ]

            # Summary
            self._report += ["{h1:>{w1}}|{h2:>{w}}|{h3:>{w}}|{h4:>{w}}|{h5:>{w}}|{h6:>{w}}|{h7:>{w}}|".format(
                w1 = first_column_len, w = column_len,
                h1 = "total",
                h2 = sums["gpus"],
                h3 = round(sums["usage_avg"]/sums["gpus"]),
                h4 = round(sums["usage_max"]),
                h5 = round(sums["mem_max"]*100/sums["mem"]),
                h6 = sums["cpu_proc_total"],
                h7 = round(int(sums["cpu_usage_max"]) / int(sums["num_cores"])),
            )]

    def _gen_io_network(self, data):
        """Build per node data of IO and network utilization."""
        nodes = data["nodes"]
        self._report += subheader("Per node IO and network. Sums and rates")
        columns = 7
        column_len = math.floor(STRLEN / columns) - 1
        first_column_len = STRLEN - (column_len + 1)*(columns - 1) - 1

        # HEADERS
        self._report += ["{h1:{w1}}|{h2:^{w}}|{h3:^{w}}|{h4:^{w}}|".format(
            h1 = " ", w1 = first_column_len, w = column_len * 2 + 1,
            h2 = "Process IO",
            h3 = "Scratch FS",
            h4 = "Infiniband",
        )]

        # HEADERS second row
        self._report += ["{h1:^{w1}}|{h2:^{w}}|{h3:^{w}}|{h4:^{w}}|{h5:^{w}}|{h6:^{w}}|{h7:^{w}}|".format(
            w1 = first_column_len, w = column_len,
            h1 = "nodes",
            h2 = "rd(sum)",
            h3 = "wr(sum)",
            h4 = "rd(sum)",
            h5 = "wr(sum)",
            h6 = "rcv[/s]",
            h7 = "snd[/s]",
        )]

        # Separator
        self._report += [
            "{}+".format("-" * first_column_len) +
            "{}+".format("-" * column_len) * (columns - 1)
        ]

        sums = {
            "read_bytes": 0,
            "write_bytes": 0,
            "scratch_read_sum": 0,
            "scratch_write_sum": 0,
            "network_rcv_max": 0,
            "network_xmit_max": 0,
        }
        for node in nodes:
            sums["read_bytes"] += node["read_bytes"]
            sums["write_bytes"] += node["write_bytes"]
            sums["scratch_read_sum"] += get_value_num_nonone(node, "beegfs_read_sum")
            sums["scratch_write_sum"] += get_value_num_nonone(node, "beegfs_write_sum")
            sums["network_rcv_max"] += get_value_num_nonone(node, "ib_rcv_max")
            sums["network_xmit_max"] += get_value_num_nonone(node, "ib_xmit_max")
            self._report += ["{h1:>{w1}}|{h2:>{w}}|{h3:>{w}}|{h4:>{w}}|{h5:>{w}}|{h6:>{w}}|{h7:>{w}}|".format(
                w1 = first_column_len, w = column_len,
                h1 = node["node_name"],
                h2 = bytes2str(node["read_bytes"]),
                h3 = bytes2str(node["write_bytes"]),
                h4 = get_value(node, "beegfs_read_sum", bytes2str),
                h5 = get_value(node, "beegfs_write_sum", bytes2str),
                h6 = get_value(node, "ib_rcv_max", bytes2str),
                h7 = get_value(node, "ib_xmit_max", bytes2str),
            )]

        if len(nodes) > 1:
            # Separator
            self._report += [
                "{}+".format("-" * first_column_len) +
                "{}+".format("-" * column_len) * (columns - 1)
            ]

            # Summary
            self._report += ["{h1:>{w1}}|{h2:>{w}}|{h3:>{w}}|{h4:>{w}}|{h5:>{w}}|{h6:>{w}}|{h7:>{w}}|".format(
                w1 = first_column_len, w = column_len,
                h1 = "total",
                h2 = bytes2str(sums["read_bytes"]),
                h3 = bytes2str(sums["write_bytes"]),
                h4 = bytes2str(sums["scratch_read_sum"]),
                h5 = bytes2str(sums["scratch_write_sum"]),
                h6 = bytes2str(sums["network_rcv_max"]),
                h7 = bytes2str(sums["network_xmit_max"]),
            )]

    def _group_nodes(self, node_data):
        node_groups = {}
        for node in node_data:
            node_hash = hash((
                node["cpu_model"],
                node["sockets"],
                node["cores_per_socket"],
                node["virt_threads_per_core"],
                round(node["available_main_mem"] / 10000000000),
            ))

            if node_hash not in node_groups:
                node_groups[node_hash] = []

            node_groups[node_hash].append(node)

        return node_groups

    def _gen_node_info(self, node_data):
        """Generate node infromation"""
        self._report += subheader("Nodes description")
        nodes = node_data
        groups = self._group_nodes(node_data)

        group_cnt = 0
        for h, g in groups.items():
            group_cnt += 1
            if group_cnt > 1:
                self._report += spacer()

            node_name_list = []
            for node in g:
                node_name_list.append(node["node_name"])

            node_name_str_compact = nodes_compact(node_name_list)

            self._report += textwrap.wrap(node_name_str_compact, STRLEN)

            self._report += sep()

            for i in [
                "cpu_model",
                "sockets",
                "cores_per_socket",
                "phys_threads_per_core",
                "virt_threads_per_core",
            ]:
                self._report += get_entry(i, g[0][i])

            self._report += get_entry(
                "available_main_mem", bytes2str(g[0]["available_main_mem"])
            )

    def _gen_recommendations(self, data):
        """Create recommendations."""
        if "recommendations" in data:
            rcms = data["recommendations"]
            if len(rcms) > 0:
                self._report += subheader("Recommendations")
                for rcm in rcms:
                    self._report += warn2str(rcm)

    def report(self):
        """
        Present the entire report.

        Returns
        -------
            string report -- the entire text report

        """
        return "\n".join(self._report)

    def errors(self):
        """Show the list of validation errors."""
        return self._val.verbose()
