#! /usr/bin/env python3
import pytest
from errors.error import SpecParserError
from errors.error import DataFormatError
from spectools.specparser import SpecParser
from spectools.spec2schema import char2bool
from spectools.spec2schema import cols2dict
from spectools.spec2schema import col2list
from spectools.spec2schema import entry2var
from spectools.spec2schema import constr2dict
from spectools.spec2schema import resolve_constraints

SNAME = "docs/spec-metrics.md"
SNAME2 = "docs/spec-metrics2.md"
TBL = {"test": {"key": "val", "key2": "val2"}}
KEY_OK = "test"
KEY_WRONG = "hello"
CONSTR_REGEX = "regex=r'[a-z]{3, 5}'"
NAME_STR = "STR"
CONSTR_INT = "min=10"
NAME_INT = "INT"
NAME_FLOAT = "FLOAT"
CONSTR_STR = "minlength=3"
NAME_MEM = "MEM"
MAPPING_VALID = {"INT": {"min": 1}}
KEY_VALID = "Constraint"


class TestSpecParser:
    @staticmethod
    def test_success():
        with open(SNAME2, "r") as fil:
            data = fil.readlines()
        assert data is not None
        spp = SpecParser(md_data=data)
        assert spp.is_valid() is True
        assert spp.get() != {}

    @staticmethod
    def test_none():
        with pytest.raises(SpecParserError):
            SpecParser(md_data=None)

    @staticmethod
    def test_empty():
        with pytest.raises(DataFormatError):
            SpecParser(md_data=[])

    @staticmethod
    def test_not_list():
        with pytest.raises(DataFormatError):
            SpecParser(md_data={})


class TestSpec2SchemaChar2Bool:
    @staticmethod
    def test_success_true():
        assert (
            char2bool("y")
            and char2bool("Y")
            and char2bool("t")
            and char2bool("T")
        ) is True

    @staticmethod
    def test_success_false():
        assert (
            not char2bool("n")
            and not char2bool("N")
            and not char2bool("f")
            and not char2bool("F")
        ) is True

    @staticmethod
    def test_not_string():
        assert char2bool([]) is None

    @staticmethod
    def test_empty():
        assert char2bool("") is None

    @staticmethod
    def test_not_covered_char():
        assert char2bool("q") is None


class TestSpec2SchemaCols2Dict:
    @staticmethod
    def test_success():
        assert cols2dict(TBL, "key", "key2") is not None

    @staticmethod
    def test_no_dict():
        assert cols2dict([], "key", "key2") is None

    @staticmethod
    def test_none_instead_dict():
        assert cols2dict(None, "key", "key2") is None

    @staticmethod
    def test_none_instead_str1():
        assert cols2dict(TBL, None, "key2") is None

    @staticmethod
    def test_none_instead_str2():
        assert cols2dict(TBL, "key", None) is None


class TestSpec2SchemaCol2List:
    @staticmethod
    def test_success():
        assert col2list(TBL, "key") is not None

    @staticmethod
    def test_none_dict():
        assert col2list(None, "key") is None

    @staticmethod
    def test_no_str():
        assert col2list(TBL, None) is None


class TestSpec2SchemaEntry2Var:
    @staticmethod
    def test_success():
        assert entry2var(TBL, varkey=KEY_OK) is not None

    @staticmethod
    def test_not_dict():
        assert entry2var(SNAME, varkey=KEY_OK) is None

    @staticmethod
    def test_varkey_not_str():
        assert entry2var(TBL, varkey=TBL) is None

    @staticmethod
    def test_varkey_missing():
        assert entry2var(TBL, varkey=KEY_WRONG) is None

    @staticmethod
    def test_dict_none():
        assert entry2var(None, varkey=KEY_OK) is None

    @staticmethod
    def test_varkey_none():
        assert entry2var(TBL, varkey=None) is None


class TestSpec2SchemaConstr2Dict:
    @staticmethod
    def test_success_regex():
        res = constr2dict(NAME_STR, CONSTR_REGEX)
        assert res is not None and isinstance(res, dict)

    @staticmethod
    def test_success_str():
        res = constr2dict(NAME_STR, CONSTR_STR)
        assert res is not None and isinstance(res, dict)

    @staticmethod
    def test_success_int():
        res = constr2dict(NAME_INT, CONSTR_INT)
        assert res is not None and isinstance(res, dict)

    @staticmethod
    def test_success_float():
        res = constr2dict(NAME_FLOAT, CONSTR_INT)
        assert res is not None and isinstance(res, dict)

    @staticmethod
    def test_success_mem():
        res = constr2dict(NAME_MEM, CONSTR_INT)
        assert res is not None and isinstance(res, dict)

    @staticmethod
    def test_wrong_name():
        res = constr2dict(KEY_OK, CONSTR_INT)
        assert res is not None and isinstance(res, dict)


class TestSpec2SchemaResolveConstraints:
    @staticmethod
    def test_success_mapping():
        entry_valid = {"Constraint": "INT"}
        res = resolve_constraints(
            entry_valid, key=KEY_VALID, mapping=MAPPING_VALID
        )
        assert res["min"] == 1

    @staticmethod
    def test_success_range():
        entry_range = {"Constraint": "Range [0, 10]"}
        res = resolve_constraints(
            entry_range, key=KEY_VALID, mapping=MAPPING_VALID
        )
        assert res["min"] == 0 and res["max"] == 10

    @staticmethod
    def test_entry_none():
        assert (
            resolve_constraints(None, key=KEY_VALID, mapping=MAPPING_VALID)
            is None
        )

    @staticmethod
    def test_entry_not_dict():
        assert (
            resolve_constraints([], key=KEY_VALID, mapping=MAPPING_VALID)
            is None
        )

    @staticmethod
    def test_key_none():
        entry_valid = {"Constraint": "INT"}
        assert (
            resolve_constraints(entry_valid, key=None, mapping=MAPPING_VALID)
        ) is None

    @staticmethod
    def test_key_not_str():
        entry_valid = {"Constraint": "INT"}
        assert (
            resolve_constraints(entry_valid, key=[], mapping=MAPPING_VALID)
        ) is None

    @staticmethod
    def test_mapping_none():
        entry_valid = {"Constraint": "INT"}
        assert (
            resolve_constraints(entry_valid, key=KEY_VALID, mapping=None)
        ) is None

    @staticmethod
    def test_mapping_not_dict():
        entry_valid = {"Constraint": "INT"}
        assert (
            resolve_constraints(entry_valid, key=KEY_VALID, mapping=[])
        ) is None
