# Report Data Input Format (WIP)

Data is expected as a single, valid JSON file. It is expected to contain all the metrics from the `spec-metrics.md` file. All keys are required. No report will be generated if a key is missing. Additional, not yet specified metrics are currently allowed, although they are not going to be displayed in the report. Following data type conversions are expected:

- `string` in the metrics specification: `string` in JSON. Constraints are not encoded in JSON, but are verified by the report.
- `integer` in the spec: `number` in JSON. Number encoded as string (written in quotes) is invalid.
- `float` in the spec: `number` in JSON. To ensure the type-safety the number has to contain a decimal point or be written in the exponential form. Number written as string is not accepted.

If some data is not available or bogus (e.g. out-of-range), only JSON `null` is allowed be used instead. Never invent own placeholders, otherwise the report will be either not generated at all or can be plainly wrong (since other values can be derived from these entries).

Entire file is an object (a dictionary). A list of elements (e.g. node-related information) is encoded as a JSON array. Job-related "general information" is in the "job" dictionary, node-related information is in the "nodes" array. Every node itself is a dictionary. At least one node object is necessary for the creation of a job report.

TODO:

- Maybe the file will be split in two (or more) to avoid loading of a large amount of time series data (node-dynamic parts) for every processing step.
- Recommendations are an optional entry. Their format has to be defined.

```json
{
    "job": {},

    "job_aggregates": {},

    "recommendations": [],

    "nodes": [
        {
            "static": {
                "name": "node1",
            },
            "dynamic": [],
            "aggregates" : {},
        },

        {
            "static": {
                "name": "node2",
            },
            "dynamic": [],
            "aggregates" : {},
        }
    ]
}
```
