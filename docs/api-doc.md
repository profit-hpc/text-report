# API docs for text-report

## Package conf

Package has no docstring.

## Module conf/config.py

Text report main configuration file.

## Package errors

Package has no docstring.

## Module errors/error.py

Exception classes for the different parts of the text report.

- Class TextReportError (base: Exception)  
  Base class for all text report exceptions.

  - Function Constructor(self, prefix, message) -> ()  
    has no docstring
- Class DataFormatError (base: TextReportError)  
  Data format related exception class.

  - Function Constructor(self, message) -> ()  
    has no docstring
- Class ValidatorError (base: TextReportError)  
  Base class for all Validator exceptions.

  - Function Constructor(self, message) -> ()  
    has no docstring
- Class SpecParserError (base: TextReportError)  
  Base class for all SpecParser exceptions.

  - Function Constructor(self, message) -> ()  
    has no docstring

## Package format

Package has no docstring.

## Module format/diagram.py

WIP: Testing plot creation in python.

- Function make\_diag(data) -> ('name string',)  
  WIP: function for building diagrams.

## Module format/latex.py

WIP: testing LaTeX report creation in python.

- Function make\_tex(title) -> ('name string',)  
  WIP: function to build the LaTeX document.

## Module format/text.py

Text report generator module.

- Function sep(symbol, length) -> ('list',)  
  Build a separator line.
- Function center\_underline(text) -> ('binary op result',)  
  Create a centered line and a separator line.
- Function header(title) -> ('binary op result',)  
  Create a report header.
- Function subheader(title) -> ('binary op result',)  
  Create a second-level header for the report.
- Function subsubheader(title) -> ('binary op result',)  
  Create a third-level header for the report.
- Function kv2str(key, val, left, right) -> ('function call result',)  
  Format a key-value pair.
- Function warn2str(warn, text, length) -> ('function call result',)  
  Format a warning.
- Function get\_entry(key, val) -> ('list',)  
  Convert a key-value pair to a formatted string, resolves key.
- Function spacer() -> ('function call result',)  
  Return an empty line.
- Class TextReport (base: Object)  
  Main class for the text report.

  - Function Constructor(self, data) -> (), raises: ('DataFormatError',)  
    has no docstring
  - Private Function \_gen\_general\_info(self, data) -> ()  
    Generate global data.
  - Private Function \_gen\_node\_data(self, data) -> ()  
    Build per node data.
  - Private Function \_gen\_recommendations(self) -> ()  
    Create recommendations.
  - Function report(self) -> ('function call result',)  
    Present the entire report.
  - Function errors(self) -> ('function call result',)  
    Show the list of validation errors.

## Module get\_spec.py

Running spec tools.

- Function cond\_print(data, verbose) -> ()  
  has no docstring

## Module runme.py

Script to validate input data and create a report.

## Package spectools

Package has no docstring.

## Module spectools/spec2schema.py

WIP: Module for converting parsed spec to validator schema.

- Function char2bool(req) -> (None,)  
  Convert characters like y/n into a boolean value.
- Function cols2dict(table, keycol, valcol) -> (None, 'dict')  
  Extract values of two columns from table and return them as dict.
- Function col2list(table, col) -> ('list', None)  
  Extract values of a column as list.
- Function entry2var(entry, varkey) -> ('tuple', None)  
  Extract metric name as the new key.
- Function constr2dict(name, constraint) -> ('name string', 'dict')  
  Split constraint string into parts and return as a dict.
- Function resolve\_constraints(entry, key, mapping) -> ('name string', None)  
  Replace constraint code with valid rules.
- Function spec2json(infile, outfile) -> ()  
  Parse spec and save parsed data as JSON.

## Module spectools/spec2tests.py

Generate tests based on parsed spec.

## Module spectools/specparser.py

Module for parsing the specification.

- Class SpecParser (base: Object)  
  Module for parsing the Markdown specifications.

  - Function Constructor(self, md\_data, table\_prefix, entry\_prefix) -> (), raises: ('DataFormatError', 'SpecParserError')  
    has no docstring
  - Private Function \_extract\_tables(self) -> ()  
    Extract tables from markdown data.
  - Private Function \_organise\_tables(self) -> ()  
    Extract table structure.
  - Function get(self) -> ('name string',)  
    Return complete parsed spec.
  - Function is\_valid(self) -> ('bool',)  
    Check if the parsing was successful.

## Module test\_all.py

Simple test module.

- Class TestJSONTools (base: Object)  
  has no docstring

  - Function test\_load\_success() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_load\_empty() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_load\_noname() -> (), annotated as: ('staticmethod',)  
    has no docstring
- Class TestCSVTools (base: Object)  
  has no docstring

  - Function test\_csv\_success() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_csv\_empty() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_csv\_noname() -> (), annotated as: ('staticmethod',)  
    has no docstring
- Class TestFileTools (base: Object)  
  has no docstring

  - Function test\_save\_str\_empty() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_save\_str\_noname() -> (), annotated as: ('staticmethod',)  
    has no docstring
- Class TestValidator (base: Object)  
  has no docstring

  - Function test\_data\_and\_json\_success() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_only\_js\_file\_success() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_only\_data\_success() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_no\_data\_no\_json() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_data\_is\_not\_dict() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_data\_empty() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_js\_empty() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_js\_invalid() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_js\_not\_string() -> (), annotated as: ('staticmethod',)  
    has no docstring
- Class TestFormat (base: Object)  
  has no docstring

  - Function test\_text\_success() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_text\_js\_invalid() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_text\_subsubheader\_success() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_diag() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_latex() -> (), annotated as: ('staticmethod',)  
    has no docstring

## Module test\_spectools.py

Module has no docstring.

- Class TestSpecParser (base: Object)  
  has no docstring

  - Function test\_success() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_none() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_empty() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_not\_list() -> (), annotated as: ('staticmethod',)  
    has no docstring
- Class TestSpec2SchemaChar2Bool (base: Object)  
  has no docstring

  - Function test\_success\_true() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_success\_false() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_not\_string() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_empty() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_not\_covered\_char() -> (), annotated as: ('staticmethod',)  
    has no docstring
- Class TestSpec2SchemaCols2Dict (base: Object)  
  has no docstring

  - Function test\_success() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_no\_dict() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_none\_instead\_dict() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_none\_instead\_str1() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_none\_instead\_str2() -> (), annotated as: ('staticmethod',)  
    has no docstring
- Class TestSpec2SchemaCol2List (base: Object)  
  has no docstring

  - Function test\_success() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_none\_dict() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_no\_str() -> (), annotated as: ('staticmethod',)  
    has no docstring
- Class TestSpec2SchemaEntry2Var (base: Object)  
  has no docstring

  - Function test\_success() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_not\_dict() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_varkey\_not\_str() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_varkey\_missing() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_dict\_none() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_varkey\_none() -> (), annotated as: ('staticmethod',)  
    has no docstring
- Class TestSpec2SchemaConstr2Dict (base: Object)  
  has no docstring

  - Function test\_success\_regex() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_success\_str() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_success\_int() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_success\_float() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_success\_mem() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_wrong\_name() -> (), annotated as: ('staticmethod',)  
    has no docstring
- Class TestSpec2SchemaResolveConstraints (base: Object)  
  has no docstring

  - Function test\_success\_mapping() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_success\_range() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_entry\_none() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_entry\_not\_dict() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_key\_none() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_key\_not\_str() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_mapping\_none() -> (), annotated as: ('staticmethod',)  
    has no docstring
  - Function test\_mapping\_not\_dict() -> (), annotated as: ('staticmethod',)  
    has no docstring

## Package tools

Package has no docstring.

## Module tools/files.py

Different file-related helper functions.

- Function load\_json(fname) -> ('name string', None)  
  Load JSON file as a dict.
- Function save\_str(fname, string) -> (None,)  
  Write a string to a file.

## Module tools/strings.py

Different string-related helper functions.

- Function sec2str(seconds) -> ('function call result',)  
  Convert seconds to a human-readable representation.
- Function bytes2str(byte) -> ('function call result',)  
  Convert bytes to kB, MB, ...
- Function center(string, length, symbol) -> ('list',)  
  Centering the string, filling up with spaces (default) or other symbols.

## Package validator

Package has no docstring.

## Module validator/validator.py

Report input data validator module.

- Class Validator (base: Object)  
  Main validator class.

  - Function Constructor(self, data, jsfile) -> (), raises: ('ValidatorError', 'DataFormatError')  
    has no docstring
  - Function is\_valid(self) -> ('bool',)  
    Return true if the data adheres to the schema, false otherwise.
  - Function verbose(self) -> ('attribute',)  
    Return error list (only useful if the data is invalid).
