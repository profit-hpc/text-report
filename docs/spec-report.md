# Text report specification

## General formatting

The ProfiT-HPC text report is so formatted that it can be viewed with console-based tools (e.g. cat or less) without breaking the layout. Thus, every output line is set to be 80 characters wide.

Report consists of multiple logical parts, distinguished by a header and a configurable separator line (default setting: dashes). Every parts contains text blocks, separated by empty lines.

A regular entry consists of a keyword and value, separated by a colon and a space. Keywords are left-aligned. Values are aligned to the longest keyword. If a value has a unit, it's displayed after the value. Time and date values are formatted according to the `YYYY-MM-DD hh:mm:ss` schema. Duration values are converted to readable form similar to `2 hrs 46 mins 40 s`.

## Report Structure

1. Title
1. General Information
1. Node Description
1. Possible Problems and Recommendations

### Title

The default title of the report is "Profit-HPC Report". It is centered. This is the only header that is prepended with an additional separator line.

### General Information

This part of the report groups general job-related information, valid for every node for the entire duration of the job.

- User name
- Project account
- Job identifier
- Used Job queue
- *\< empty line as separator \>*
- Job submit time
- Job start time
- Job end time
- Requested time
- Used time
- *\< empty line as separator \>*
- Requested cores
- Number of used nodes

### Node Description

Currently, the complete information is displayed for every node. Later, it will be aggregated, so that only unique configurations and their counter is shown.

- Node name
- CPU model
- Sockets per node
- Cores per socket
- phys. Threads per core
- virt. Threads per core
- Available RAM
- *\< empty line as separator \>*
- Number of used cores
- CPU usage
- CPU time user
- CPU time system
- CPU time idle
- CPU time iowait
- *\< empty line as separator \>*
- Mem RSS max
- Mem RSS avg
- Mem VMS
- Swap max
- Available Main Mem
- *\< empty line as separator \>*
- Read bytes
- Write bytes
- Read count
- Write count

### Possible Problems and Recommendations

This section will hold pecularities noticed on the basis of collected metrics and information. Each pecularity will only be shown when the defined conditions are met.
Every entry will contain following information:

- Problem ID
- Problem category
- Condition
- Short statement on the problem.

Using the Problem ID, it will be possible to get a more detailed explanation from the user guide and on our homepage, to better understand what these pecularities mean and (if possible) how the user can react to them.

## Example report with dummy values

```text
--------------------------------------------------------------------------------
                                ProfiT-HPC Report
--------------------------------------------------------------------------------

                               General Information
--------------------------------------------------------------------------------
User Name:                            user1
Project Account:                      n/a
JobID:                                2368599
Used Queue:                           queue1

Job submitted at:                     01 Oct 2018 12:30:00
Job started at:                       01 Oct 2018 12:30:00
Job finished at:                      01 Oct 2018 15:30:00
Requested Runtime:                    3:00:00
Used Walltime:                        1:21:25

Used Nodes:                           3
Requested Compute Units:              13

                                Node description
--------------------------------------------------------------------------------
Node Name:                            node1
CPU Vendor and Model:                 Intel(R) Xeon(R) CPU E5-2650 v3 @ 2.30GHz
Number of CPU Sockets:                2
Number of Cores per Socket:           10
Number of HW Threads per Core:        1
Number of Virtual Threads per Core:   0
Available Memory:                     62.81 GiB

Used Cores:                           5
CPU load:                             100
Runtime (User) of the Job:            1:12:21
Runtime (System) of the Job:          0:00:05
Idle Time of the Job:                 0:00:00
IOWait Time of the Job:               0:00:00

Maximum RSS memory used by Job:       141.88 MiB
Average RSS memory used by Job:       96.33 MiB
Maximum SWAP used by Job:             0 B
Virtual Memory size allocated by Job: 716.65 MiB
Available Memory:                     62.81 GiB

Data read from the disk:              504.0 KiB
Data written to disk:                 8.0 KiB
Number of disk reads:                 43774
Number of disk writes:                5531
--------------------------------------------------------------------------------
Node Name:                            node0
CPU Vendor and Model:                 Intel(R) Xeon(R) CPU E5-2650 v3 @ 2.30GHz
Number of CPU Sockets:                2
Number of Cores per Socket:           10
Number of HW Threads per Core:        1
Number of Virtual Threads per Core:   0
Available Memory:                     62.81 GiB

Used Cores:                           5
CPU load:                             100
Runtime (User) of the Job:            1:12:10
Runtime (System) of the Job:          0:00:15
Idle Time of the Job:                 0:00:00
IOWait Time of the Job:               0:00:00

Maximum RSS memory used by Job:       118.8 MiB
Average RSS memory used by Job:       54.24 MiB
Maximum SWAP used by Job:             0 B
Virtual Memory size allocated by Job: 258.51 MiB
Available Memory:                     62.81 GiB

Data read from the disk:              568.0 KiB
Data written to disk:                 0 B
Number of disk reads:                 476
Number of disk writes:                748
--------------------------------------------------------------------------------
Node Name:                            node2
CPU Vendor and Model:                 Intel(R) Xeon(R) CPU E5-2650 v3 @ 2.30GHz
Number of CPU Sockets:                2
Number of Cores per Socket:           10
Number of HW Threads per Core:        1
Number of Virtual Threads per Core:   0
Available Memory:                     62.81 GiB

Used Cores:                           3
CPU load:                             100
Runtime (User) of the Job:            1:12:11
Runtime (System) of the Job:          0:00:14
Idle Time of the Job:                 0:00:00
IOWait Time of the Job:               0:00:00

Maximum RSS memory used by Job:       42.85 MiB
Average RSS memory used by Job:       38.38 MiB
Maximum SWAP used by Job:             0 B
Virtual Memory size allocated by Job: 177.82 MiB
Available Memory:                     62.81 GiB

Data read from the disk:              396.0 KiB
Data written to disk:                 0 B
Number of disk reads:                 439
Number of disk writes:                500
--------------------------------------------------------------------------------

                      Possible Problems and Recommendations
--------------------------------------------------------------------------------
ATTENTION: E0001, Walltime, 1:21:25 < 0.5 * 3:00:00,
Your application only took a fraction of the requested walltime.  Request less
walltime to speed up your submissions and save resources.

ATTENTION: E0002, Compute Units, 13 < 0.9 * 60,
Your application only used a fraction of available compute units (cores) on
every node, maybe you could allocate fewer nodes next time.

```
