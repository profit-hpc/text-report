# Minimal set of metrics and information

The ProfiT-HPC text report needs a minimal set of information and metrics to be created.
The following document specifies the minimal set of metrics and their formats expected by the text-report generator.
The first part is general information on the running job, identical for all used compute nodes and only needs to be collected once.
The second set handles information and metrics that might differ on the different compute nodes.
Accordingly, this set of information and metrics will have to be collected on each compute node.

Following conventions are used in the specification:

- In the context of this tool, a `standard string` is limited to upper- and lower-case ASCII characters, numbers, and underscores. It can be between 3 and 45 characters long.
- An `extended string` can also contain whitespace and other characters (e.g. punctuation, '@' or parentheses)
- `integer` is a positive or negative whole number or a zero. Its range can (and should) be limited by additional constraints.
- `UNIX timestamp` is an integer, representing the number of seconds since 01.01.1970, minimum value is 1,000,000,000 (was on 09.09.2001, so we should get larger values)

## Job-based, General Information

| Keyword | Type | Add. Constraints | Additional Information |
| ------- | ---- | ---------------- | ---------------------- |
| `job_id` | `standard string` | | JobID given to the job by the batch system |
| `user_name` | `standard string` | |  Name of the User running the job |
| `used_queue` | `standard string` | | Name of the queue in which the job ran |
| `submit_time` | `UNIX timestamp` | | |
| `start_time` | `UNIX timestamp` | | |
| `end_time` | `UNIX timestamp` | | |
| `requested_time` | `integer` in seconds | range [1, 31556952] | one year as max value |
| `requested_cu` | `integer` | range [1, 100000000] | number of requested compute units |
| `num_nodes` | `integer` | range [1, 10000000] | number of nodes the job ran on |

## Job-based, Node-specific Information and Metrics

Regarding the threads per core: since most CPU architectures in our compute centers are Intel/AMD, we adapted the following convention: `physical` is set to 1 in every case, `virtual` is 1 if HT or similar technology is used. The total `threads per core` value is the sum of these two. For Power8 architecture for example, the `physical` number of threads can be up to 8, `virtual` is 0.

Please tell us if this convention is not sufficient for you.

| Keyword | Type | Add. Constraints | Additional Information |
| --------------- | ------- | ------------ | ------------------------------- |
| `node_name` | `standard string` | | name of the node as given by the batch system |
| `cpu_model` | `extended string` | | CPU model name |
| `available_main_mem` | `integer` in bytes | [100MB, 100TB] plausibility range | the available main memory per node |
| `sockets` | `integer`| range [1, 16] | available number of CPU sockets on node |
| `cores_per_socket` | `integer` | range [1, 256] | available number of cores per socket |
| `phys_threads_per_core` | `integer` | range [1, 256] | available number of physical threads per core |
| `virt_threads_per_core` | `integer` | range [0, 256] | available number of virtual threads per core |
| `cpu_time_user` | `integer` in seconds | >= 0 | sum of CPU user time of the job |
| `cpu_time_system` | `integer` in seconds | >= 0 | sum of CPU sys time of the job |
| `cpu_time_idle` | `integer` in seconds | >= 0 | sum of CPU idle time of the job |
| `cpu_time_iowait` | `integer` in seconds | >= 0 | sum of CPU iowait time of the job |
| `num_cores` | `integer` | > 0 | number of cores assigned to job per node |
| `mem_rss_max` | `integer` in bytes | [10MB, 10TB] plausibility range | maximum RSS memory used by job per node |
| `mem_rss_avg` | `integer` in bytes | [10MB, 10TB] plausibility range | average RSS memory used by job per node |
| `mem_swap_max` | `integer` in bytes | >= 0 | maximum swap memory used by job per node |
| `mem_vms` | `integer` in bytes | [10MB, 10TB] plausibility range | Virtual Memory size allocated by job per node.  It includes all memory that the process can access, including memory that is swapped out, memory that is allocated, but not used, and memory that is from shared libraries. |
