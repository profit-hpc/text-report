#! /usr/bin/env python3
"""Running spec tools."""

import json
from pprint import pprint as pp
from spectools.spec2schema import spec2json
from spectools.spec2schema import JSONFILE
from spectools.spec2schema import col2list
from spectools.spec2schema import cols2dict
from spectools.spec2schema import entry2var
from spectools.spec2schema import char2bool
from spectools.spec2schema import constr2dict
from spectools.spec2schema import resolve_constraints


VERBOSE = False


def cond_print(data, verbose=VERBOSE):
    """Print data depending on type."""
    if verbose:
        if isinstance(data, str):
            print(data)
        else:
            pp(data)


if __name__ == "__main__":
    # ------ parsing specification --------
    spec2json()

    # ------ interpreting tables -------
    with open(JSONFILE, "r") as f:
        DATA = json.load(f)
    cond_print(DATA.keys())
    TNAMES = col2list(DATA["table0"], "JSON name")
    cond_print(TNAMES)
    CONSTRAINTS = cols2dict(DATA["table1"], "Abbreviation", "Constraint")
    cond_print(CONSTRAINTS)
    CONSTRAINTS = {k: constr2dict(k, v) for k, v in CONSTRAINTS.items()}
    cond_print(CONSTRAINTS)
    METRICS = {TNAMES[pos]: DATA[key] for pos, key in enumerate([*DATA][3:])}

    # -------- generating schema -----------
    SCHEMA = dict()
    for tname in METRICS:
        table = dict()
        for entry in METRICS[tname]:
            current = METRICS[tname][entry]
            key, val = entry2var(current)
            if "Required" in val:
                val["required"] = char2bool(val.pop("Required"))
            if "Constraint" in val:
                val = resolve_constraints(val, mapping=CONSTRAINTS)
            if "Data type" in val:
                val["type"] = val.pop("Data type")
            if "Unit" in val:
                del val["Unit"]
            if "Label / Caption" in val:
                del val["Label / Caption"]
            if key not in table:
                table[key] = val
            else:
                cond_print("Duplicate key: {}".format(key))
        SCHEMA[tname] = table

    cond_print(SCHEMA, True)
