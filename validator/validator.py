#! /usr/bin/env python3
"""Report input data validator module."""

import io
import json
import cerberus
from errors.error import ValidatorError
from errors.error import DataFormatError
from tools.files import load_json
from conf.config import SCHEMA


class Validator:
    """
    Main validator class.

    Expects
    -------
        either dictionary or a json file containing the data
        that needs to be checked against the schema

    Raises
    ------
        DataFormatError -- if the input data is not a dictionary
        ValidatorError -- if the given file is bogus

    """

    def __init__(self, data=None, jsfile=None):
        if data is not None:
            if not isinstance(data, dict):
                raise DataFormatError("Data has to be a dictionary ...")
            else:
                self.val = cerberus.Validator()
                self.val.allow_unknown = True
                self.val.validate(data, SCHEMA)
        elif jsfile is not None:
            # in the next version move this to the load_json function
            self.val = cerberus.Validator()
            self.val.allow_unknown = True
            if isinstance(jsfile, str):
                self.val.validate(load_json(jsfile), SCHEMA)
            elif isinstance(jsfile, io.IOBase):
                self.val.validate(json.load(jsfile), SCHEMA)
            else:
                raise ValidatorError("Expected file name or file handler ...")
        else:
            raise ValidatorError("Either data or json file is required ...")

    def is_valid(self):
        """Return true if the data adheres to the schema, false otherwise."""
        return self.val.errors is None or self.val.errors == {}

    def verbose(self):
        """Return error list (only useful if the data is invalid)."""
        return self.val.errors


# def test_validator(fname):
#     """
#     Simple check if the given file name contains valid data.

#     Arguments
#     ---------
#         string fname -- json file containing the data

#     Returns
#     -------
#         true -- if the data is valid

#     """
#     val = Validator(jsfile=fname)
#     return val.is_valid()


# if __name__ == "__main__":
#     if test_validator("../testdata/minset.json"):
#         print("Testfile complies to the schema definition...")
#     else:
#         print("Testfile doesn't comply to the schema definition...")
