#! /usr/bin/env python3
"""Exception classes for the different parts of the text report."""

TEMPLATE = "ERROR({}):{}"


class TextReportError(Exception):
    """Base class for all text report exceptions."""

    def __init__(self, prefix, message):
        self.message = TEMPLATE.format(prefix, message)
        super().__init__()


class DataFormatError(TextReportError):
    """Data format related exception class."""

    def __init__(self, message):
        super().__init__("Data Format", message)


class ValidatorError(TextReportError):
    """Base class for all Validator exceptions."""

    def __init__(self, message):
        super().__init__("Validator", message)


class SpecParserError(TextReportError):
    """Base class for all SpecParser exceptions."""

    def __init__(self, message):
        super().__init__("SpecParser", message)
