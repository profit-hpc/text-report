#! /usr/bin/env python3
"""Text report main configuration file."""


# Data Validation
STR_MIN = 2
STR_MAX = 45
# only lower-case and numbers, no whitespace
LOW_NO_WS = r"[a-z0-9-._]+"
UPLO_NO_WS = r"[a-zA-Z0-9-._\+]+"  # both cases, numbers, no whitespace
# both cases, numbers, space allowed
UPLO_WITH_WS = r"[a-zA-Z0-9_ \(\)@\.,\-]+"
STR_DEFAULT = {
    "type": "string",
    "regex": UPLO_NO_WS,
    "minlength": STR_MIN,
    "maxlength": STR_MAX,
    "required": True,
}
MIO = 1000 * 1000  # one million
INT_TS = 1000 * MIO  # was on 9 Sep 2001, so we should get larger values
DUR_MAX = 31556952  # exactly one year, maybe use 365 * 24 * 60 * 60
CU_MAX = 100 * MIO  # 100 Mio should be future-proof
NODE_MAX = 10 * MIO  # 10 Mio nodes - again, future-proof
MB = 1024 * 1024  # 1 megabyte
MEM_MIN_TOTAL = 100 * MB  # less than 100MB is not plausible
# 100TB on a single node should be safe for now
MEM_MAX_TOTAL = MEM_MIN_TOTAL * 1024 * 1024
MEM_MIN_PROC = 0
MEM_MAX_PROC = 10 * MB * 1024 * 1024
SAMPL = r"[0-9]+ [h\|m\|s]"

SCHEMA = {
    "job_id": STR_DEFAULT,
    "general": {
        "type": "dict",
        "keyschema": {
            "type": "string",
            "required": True,
            "regex": LOW_NO_WS,
            "minlength": STR_MIN,
        },
        "schema": {
            "user_name": STR_DEFAULT,
            "used_queue": STR_DEFAULT,
            "submit_time": {
                "type": "integer",
                "required": True,
                "min": INT_TS,
            },
            "start_time": {"type": "integer", "required": True, "min": INT_TS},
            "end_time": {"type": "integer", "required": True, "min": INT_TS},
            "requested_time": {
                "type": "integer",
                "required": True,
                "min": 1,
                "max": DUR_MAX,
            },
            "requested_cu": {
                "type": "integer",
                "required": True,
                "min": 1,
                "max": CU_MAX,
            },
            "num_nodes": {
                "type": "integer",
                "required": True,
                "min": 1,
                "max": NODE_MAX,
            },
        },
    },
    "nodes": {
        "type": "list",
        "empty": False,
        "required": True,
        "schema": {
            "type": "dict",
            "keyschema": {
                "type": "string",
                "regex": LOW_NO_WS,
                "minlength": STR_MIN,
            },
            "schema": {
                "node_name": STR_DEFAULT,
                "cpu_model": {
                    "type": "string",
                    "required": True,
                    "regex": UPLO_WITH_WS,
                    "minlength": STR_MIN,
                    "maxlength": STR_MAX,
                },
                "available_main_mem": {
                    "type": "integer",
                    "required": True,
                    "min": MEM_MIN_TOTAL,
                    "max": MEM_MAX_TOTAL,
                },
                "sockets": {
                    "type": "integer",
                    "required": True,
                    "min": 1,
                    "max": 16,
                },
                "cores_per_socket": {
                    "type": "integer",
                    "required": True,
                    "min": 1,
                    "max": 256,
                },
                "phys_threads_per_core": {
                    "type": "integer",
                    "required": True,
                    "min": 1,
                    "max": 256,
                },
                "virt_threads_per_core": {
                    "type": "integer",
                    "required": True,
                    "min": 0,
                    "max": 256,
                },
                "cpu_time_user": {"type": "float", "required": True, "min": 0},
                "cpu_time_system": {
                    "type": "float",
                    "required": True,
                    "min": 0,
                },
                "cpu_time_idle": {"type": "float", "required": True, "min": 0},
                "cpu_time_iowait": {
                    "type": "float",
                    "required": True,
                    "min": 0,
                },
                "num_cores": {"type": "integer", "required": True, "min": 1},
                "mem_rss_max": {
                    "type": "integer",
                    "required": True,
                    "min": MEM_MIN_PROC,
                    "max": MEM_MAX_PROC,
                },
                "mem_rss_avg": {
                    "type": "integer",
                    "required": True,
                    "min": MEM_MIN_PROC,
                    "max": MEM_MAX_PROC,
                },
                "mem_swap_max": {"type": "integer", "min": 0},
                "mem_vms": {
                    "type": "integer",
                    "required": True,
                    "min": MEM_MIN_PROC,
                    "max": MEM_MAX_PROC,
                },
                "ib_rcv_max": {"type": "integer", "required": False, "nullable": True},
                "ib_xmit_max": {"type": "integer", "required": False, "nullable": True},
                "read_bytes": {"type": "integer", "required": False, "nullable": True},
                "write_bytes": {"type": "integer", "required": False, "nullable": True},
            },
        },
    },
}

TS_ENTRY = {
    "static": {
        "type": "dict",
        "keyschema": {
            "type": "string",
            "required": True,
            "regex": LOW_NO_WS,
            "minlength": STR_MIN,
        },
        "schema": {
            "pfit_timestamp": {"type": "integer", "min": 1, "required": True},
            "pfit_cpu_time_user": {
                "type": "integer",
                "min": 0,
                "required": True,
            },
            "pfit_cpu_time_system": {
                "type": "integer",
                "min": 0,
                "required": True,
            },
            "pfit_cpu_time_idle": {
                "type": "integer",
                "min": 0,
                "required": True,
            },
            "pfit_cpu_time_iowait": {
                "type": "integer",
                "min": 0,
                "required": False,
            },
            "pfit_mem_rss": {
                "type": "integer",
                "required": True,
                "min": MEM_MIN_PROC,
                "max": MEM_MAX_PROC,
            },
            "pfit_used_swap_size": {
                "type": "integer",
                "min": 0,
                "required": True,
            },
            "pfit_fs_read_bytes": {
                "type": "integer",
                "min": 0,
                "required": False,
            },
            "pfit_fs_write_bytes": {
                "type": "integer",
                "min": 0,
                "required": False,
            },
            "pfit_fs_read_count": {
                "type": "integer",
                "min": 0,
                "required": False,
            },
            "pfit_fs_write_count": {
                "type": "integer",
                "min": 0,
                "required": False,
            },
            "pfit_num_threads": {
                "type": "integer",
                "min": 0,
                "required": False,
            },
            "pfit_num_processes": {
                "type": "integer",
                "min": 0,
                "required": True,
            },
            "pfit_load1": {"type": "float", "min": 0, "required": False},
            "pfit_total_context_switches": {
                "type": "integer",
                "min": 0,
                "required": False,
            },
        },
    }
}

NODE = {
    "static": {
        "type": "dict",
        "keyschema": {
            "type": "string",
            "required": True,
            "regex": LOW_NO_WS,
            "minlength": STR_MIN,
        },
        "schema": {
            "pfit_node_name": {
                "type": "string",
                "regex": UPLO_NO_WS,
                "minlength": STR_MIN,
                "maxlength": STR_MAX,
                "required": True,
            },
            "pfit_cpu_model": {
                "type": "string",
                "regex": UPLO_WITH_WS,
                "minlength": STR_MIN,
                "maxlength": STR_MAX,
                "required": True,
            },
            "pfit_available_main_mem": {
                "type": "integer",
                "min": MEM_MIN_PROC,
                "max": MEM_MAX_PROC,
                "required": True,
            },
            "pfit_mem_latency": {"type": "float", "min": 0, "required": False},
            "pfit_mem_bw": {"type": "float", "min": 0, "required": False},
            "pfit_sockets_per_node": {
                "type": "integer",
                "required": True,
                "min": 1,
                "max": 16,
            },
            "pfit_cores_per_socket": {
                "type": "integer",
                "required": True,
                "min": 1,
                "max": 1024,
            },
            "pfit_phys_threads_per_core": {
                "type": "integer",
                "required": True,
                "min": 1,
                "max": 1024,
            },
            "pfit_virt_threads_per_core": {
                "type": "integer",
                "required": True,
                "min": 0,
                "max": 1024,
            },
            "pfit_cache_l1i_size": {
                "type": "integer",
                "required": True,
                "min": 1,
            },
            "pfit_cache_l1d_size": {
                "type": "integer",
                "required": True,
                "min": 1,
            },
            "pfit_cache_l2_size": {
                "type": "integer",
                "required": True,
                "min": 1,
            },
            "pfit_cache_l3_size": {
                "type": "integer",
                "required": True,
                "min": 0,
            },
            "pfit_assigned_cpus": {
                "type": "integer",
                "min": 1,
                "required": True,
            },
            "pfit_max_alloc_mem": {
                "type": "integer",
                "min": MEM_MIN_PROC,
                "max": MEM_MAX_PROC,
                "required": False,
            },
            "pfit_requested_mem": {
                "type": "integer",
                "min": 0,
                "required": True,
            },
        },
    },
    "dynamic": {
        "type": "list",
        "empty": False,
        "required": True,
        "schema": {
            "type": "dict",
            "keyschema": {
                "type": "string",
                "regex": LOW_NO_WS,
                "minlength": STR_MIN,
            },
            "schema": TS_ENTRY,
        },
    },
    "node_aggregates": {
        "type": "dict",
        "keyschema": {
            "type": "string",
            "required": True,
            "regex": LOW_NO_WS,
            "minlength": STR_MIN,
        },
        "schema": {
            "pfit_num_processes_node_min": {
                "type": "integer",
                "min": 1,
                "required": True,
            },
            "pfit_num_processes_node_max": {
                "type": "integer",
                "min": 1,
                "required": True,
            },
            "pfit_num_processes_node_avg": {
                "type": "integer",
                "min": 1,
                "required": True,
            },
            "pfit_num_processes_node_median": {
                "type": "integer",
                "min": 1,
                "required": True,
            },
            "pfit_num_threads_node_min": {
                "type": "integer",
                "min": 1,
                "required": False,
            },
            "pfit_num_threads_node_max": {
                "type": "integer",
                "min": 1,
                "required": False,
            },
            "pfit_num_threads_node_avg": {
                "type": "integer",
                "min": 1,
                "required": False,
            },
            "pfit_num_threads_node_median": {
                "type": "integer",
                "min": 1,
                "required": False,
            },
            "pfit_mem_rss_node_avg": {
                "type": "integer",
                "min": MEM_MIN_PROC,
                "max": MEM_MAX_PROC,
                "required": True,
            },
            "pfit_mem_rss_node_max": {
                "type": "integer",
                "min": MEM_MIN_PROC,
                "max": MEM_MAX_PROC,
                "required": True,
            },
            "pfit_used_swap_node_max": {
                "type": "integer",
                "min": 0,
                "required": True,
            },
        },
    },
}

SCHEMA2 = {
    "general": {
        "type": "dict",
        "keyschema": {
            "type": "string",
            "required": True,
            "regex": LOW_NO_WS,
            "minlength": STR_MIN,
        },
        "schema": {
            "pfit_job_id": {
                "type": "string",
                "regex": UPLO_NO_WS,
                "minlength": STR_MIN,
                "maxlength": STR_MAX,
                "required": True,
            },
            "pfit_user_name": {
                "type": "string",
                "regex": UPLO_NO_WS,
                "minlength": STR_MIN,
                "maxlength": STR_MAX,
                "required": True,
            },
            "pfit_project_account": {
                "type": "string",
                "regex": UPLO_NO_WS,
                "minlength": STR_MIN,
                "maxlength": STR_MAX,
                "required": False,
            },
            "pfit_used_queue": {
                "type": "string",
                "regex": UPLO_NO_WS,
                "minlength": STR_MIN,
                "maxlength": STR_MAX,
                "required": True,
            },
            "pfit_submit_time": {
                "type": "integer",
                "required": True,
                "min": INT_TS,
            },
            "pfit_start_time": {
                "type": "integer",
                "required": True,
                "min": INT_TS,
            },
            "pfit_end_time": {
                "type": "integer",
                "required": True,
                "min": INT_TS,
            },
            "pfit_requested_time": {
                "type": "integer",
                "required": True,
                "min": 1,
                "max": DUR_MAX,
            },
            "pfit_requested_cores": {
                "type": "integer",
                "required": True,
                "min": 1,
            },
            "pfit_num_used_nodes": {
                "type": "integer",
                "required": True,
                "min": 1,
            },
            "pfit_sampling_interval": {
                "type": "string",
                "regex": r"[0-9]+[h|m|s]",
                "minlength": 2,
                "maxlength": 10,
                "required": True,
            },
            "pfit_return_value": {
                "type": "integer",
                "min": 0,
                "max": 1,
                "required": False,
            },
        },
    },
    "nodes": {
        "type": "list",
        "empty": False,
        "required": True,
        "schema": {
            "type": "dict",
            "keyschema": {
                "type": "string",
                "regex": LOW_NO_WS,
                "minlength": STR_MIN,
            },
            "schema": NODE,
        },
    },
    "job_aggregates": {
        "type": "dict",
        "keyschema": {
            "type": "string",
            "required": True,
            "regex": LOW_NO_WS,
            "minlength": STR_MIN,
        },
        "schema": {
            "pfit_used_swap_job": {
                "type": "integer",
                "min": 0,
                "max": 1,
                "required": True,
            }
        },
    },
}


# Report formatting
COLS = [
    "load_1",
    "load_5",
    "load_15",
    "last_pid",
    "cpu_mhz",
    "memfree",
    "buffers",
    "cached",
    "inactive(anon)",
    "swapfree",
    "numa_hit",
    "numa_miss",
    "numa_foreign",
    "pswpin",
    "pswpout",
]

CLEARNAME_MAPPING = {
    "user_name": "User Name",
    "used_queue": "Used Queue/Partition",
    "node_name": "Node Name",
    "job_id": "JobID",
    "used_time": "Elapsed wall clock time",
    "proj_acc": "Project Account",
    "exit_code": "Exit Code",
    # times
    "requested_time": "Requested wall clock time",
    "submit_time": "Job submitted at",
    "start_time": "Job started at",
    "end_time": "Job finished at",
    "cpu_time_user": "Runtime (User) of the Job",
    "cpu_time_system": "Runtime (System) of the Job",
    "cpu_time_idle": "Idle Time of the Job",
    "cpu_time_iowait": "IOWait Time of the Job",
    # HW
    "requested_cu": "Requested Compute Units",
    "num_nodes": "Used Nodes",
    "cpu_model": "CPU Vendor and Model",
    "sockets": "Number of CPU Sockets",
    "cores_per_socket": "Number of Cores per Socket",
    "phys_threads_per_core": "Number of HW Threads per Core",
    "virt_threads_per_core": "Number of hyperthreads per Core",
    "num_cores": "Used Cores",
    # memory
    "available_main_mem": "Available Memory",
    "mem_rss_max": "Maximum RSS memory used by Job",
    "mem_rss_avg": "Average RSS memory used by Job",
    "mem_swap_max": "Maximum SWAP used by Job",
    "mem_vms": "Virtual Memory size allocated by Job",
    # IO
    "read_bytes": "Data read from the disk",
    "write_bytes": "Data written to disk",
    "read_count": "Number of disk reads",
    "write_count": "Number of disk writes",
    # Load
    "cpu_usage": "CPU load",
}

STRLEN = 80
NAME_COL = max([len(i) + 2 for i in CLEARNAME_MAPPING.values()])
VALUE_COL = STRLEN - NAME_COL
