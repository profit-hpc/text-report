#! /usr/bin/env python3
"""Different string-related helper functions."""

import math
from datetime import timedelta

from conf.config import STRLEN


def sec2str(seconds):
    """
    Convert seconds to a human-readable representation.

    Arguments
    ---------
        integer seconds -- number of seconds

    Returns
    -------
        string -- amount of days, hours, etc.

    """
    if seconds is None:
        return None

    return str(timedelta(seconds=seconds))


def bytes2str(byte):
    """
    Convert bytes to kB, MB, ...

    Arguments
    ---------
        integer byte -- byte value

    Returns
    -------
        string -- value in kB, MB, GB etc.

    """
    if byte is None:
        byte = 0

    units = ["B", "KiB", "MiB", "GiB", "TiB"]

    res = ""
    if byte < 1024:
        res = "{} {}".format(byte, "B")
    else:
        factor = math.floor(math.log(byte, 1024))
        val = round(byte / (1024 ** factor), 2)
        res = "{0:.2f} {1}".format(val, units[factor])

    return res

def bytes2gib(byte):
    """
    Convert bytes to GiB, ...

    Arguments
    ---------
        integer byte -- byte value

    Returns
    -------
        string -- value in GiB, with 2 decimal places

    """
    if byte is None:
        return None

    factor = 3 # GiB factor of KiB
    val = round(byte / (1024 ** factor), 2)

    return "{0:.2f}".format(val)

def ecode2str(exit_code):
    """
    Convert exit code to human friendly format

    Arguments
    ---------
        integer code -- exit code

    Returns
    -------
        string -- exit status (with code)

    """
    estatus = "Unknown"
    exit_code = int(exit_code)

    if exit_code == 0:
        estatus = "Success (0)"
    elif exit_code > 0:
        estatus = "Failure ({:d})".format(exit_code)

    return estatus


def center(string, length=STRLEN, symbol=" "):
    """
    Centering the string, filling up with spaces (default) or other symbols.

    Arguments
    ---------
        string -- text string

    Keyword Arguments
    -----------------
        integer length -- length of the containing string
                          (should be larger than text)
        character symbol -- characters to fill the empty space
                            left and right from text

    Returns
    -------
        formatted string

    """
    return [string.center(length, symbol)]
