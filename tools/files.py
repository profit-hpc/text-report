#! /usr/bin/env python3
"""Different file-related helper functions."""

import os
from json import load
from json.decoder import JSONDecodeError

# from errors.error import DataFormatError


# def save_list(fname, datalist):
#     """
#     Save list to file.

#     Arguments
#     ---------
#         fname -- File name to write to.
#         datalist -- Data as List.

#     Raises
#     ------
#         DataFormatError -- if no list is passed to the function.

#     """
#     if not isinstance(datalist, list):
#         raise DataFormatError("Parameter is not a list ...")
#     outfile = os.path.abspath(fname)
#     path = os.path.dirname(outfile)
#     os.makedirs(path, exist_ok=True)
#     with open(outfile, "w") as fil:
#         fil.write("\n".join(datalist))

def load_json(fname):
    """
    Load JSON file as a dict.

    Arguments
    ---------
        Name of the JSON file.

    Returns
    -------
        According Python data structure or None.

    """
    infile = os.path.abspath(fname)
    try:
        with open(infile, "r") as fil:
            doc = load(fil)
            return doc
    except JSONDecodeError:
        return None
    except IsADirectoryError:
        return None


def save_str(fname, string):
    """
    Write a string to a file.

    Arguments
    ---------
        fname -- Name of the file to write to.
        string -- Data to save to the file.

    """
    outfile = os.path.abspath(fname)
    path = os.path.dirname(outfile)
    os.makedirs(path, exist_ok=True)
    try:
        with open(outfile, "w") as fil:
            fil.write(string)
        return True
    except IsADirectoryError:
        return False
