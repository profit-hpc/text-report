#! /usr/bin/env python3
"""Simple test module."""

import pytest
from cerberus.validator import DocumentError
from errors.error import ValidatorError
from errors.error import DataFormatError
from tools.files import save_str
from tools.files import load_json
from tools.files import load_csv
from format.text import TextReport
from format.text import subsubheader
from format.diagram import make_diag
from format.latex import make_tex
from validator.validator import Validator


RNAME = "out/test.txt"
DNAME = "testdata/stats.csv"
INAME = "out/example.png"
JS_EMPTY = "testdata/empty.json"
JS_SCHEMA_INVALID = "testdata/minset-invalid.json"
JS_SCHEMA_VALID = "testdata/newset-valid.json"
TESTSTRING = "test test test"


class TestJSONTools:
    @staticmethod
    def test_load_success():
        data = load_json(JS_SCHEMA_VALID)
        assert data is not None
        assert data["general"]["start_time"] == 1538397000

    @staticmethod
    def test_load_empty():
        data = load_json(JS_EMPTY)
        assert data is None

    @staticmethod
    def test_load_noname():
        data = load_json("")
        assert data is None


class TestCSVTools:
    @staticmethod
    def test_csv_success():
        data = load_csv(DNAME)
        assert data is not None

    @staticmethod
    def test_csv_empty():
        data = load_csv(JS_EMPTY)
        assert data is None

    @staticmethod
    def test_csv_noname():
        data = load_csv("")
        assert data is None


class TestFileTools:
    @staticmethod
    def test_save_str_empty():
        assert save_str(RNAME, "") is True

    @staticmethod
    def test_save_str_noname():
        assert save_str("", TESTSTRING) is False


class TestValidator:
    @staticmethod
    def test_data_and_json_success():
        data = load_json(JS_SCHEMA_VALID)
        val = Validator(data=data, jsfile=JS_SCHEMA_VALID)
        assert val.is_valid() is True

    @staticmethod
    def test_only_js_file_success():
        with open(JS_SCHEMA_VALID, "r") as infile:
            val = Validator(jsfile=infile)
            assert val.is_valid() is True

    @staticmethod
    def test_only_data_success():
        data = load_json(JS_SCHEMA_VALID)
        val = Validator(data=data)
        assert val.is_valid() is True

    @staticmethod
    def test_no_data_no_json():
        with pytest.raises(ValidatorError):
            Validator(data=None)

    @staticmethod
    def test_data_is_not_dict():
        with pytest.raises(DataFormatError):
            Validator(data=[])

    @staticmethod
    def test_data_empty():
        val = Validator(data={})
        assert val.is_valid() is False
        assert [] != val.verbose()

    @staticmethod
    def test_js_empty():
        with pytest.raises(DocumentError):
            Validator(jsfile=JS_EMPTY)

    @staticmethod
    def test_js_invalid():
        with pytest.raises(DocumentError):
            Validator(jsfile=DNAME)

    @staticmethod
    def test_js_not_string():
        with pytest.raises(ValidatorError):
            Validator(jsfile=[])


class TestFormat:
    @staticmethod
    def test_text_success():
        # FIXME: bad test, rewrite
        data = load_json(JS_SCHEMA_VALID)
        rep = TextReport(data)
        assert [] != rep.report()
        assert {} == rep.errors()

    @staticmethod
    def test_text_js_invalid():
        data = load_json(JS_SCHEMA_INVALID)
        with pytest.raises(DataFormatError):
            TextReport(data)

    @staticmethod
    def test_text_subsubheader_success():
        assert subsubheader("hello") == [
            "",
            " " * 34 + "** hello **" + " " * 35,
        ]

    @staticmethod
    def test_diag():
        data = load_csv(DNAME)
        fig = make_diag(data)
        assert fig is not None

    @staticmethod
    def test_latex():
        doc = make_tex(TESTSTRING)
        assert doc is not None
