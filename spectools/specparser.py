#! /usr/bin/env python3
"""Module for parsing the specification."""
from errors.error import SpecParserError
from errors.error import DataFormatError


class SpecParser:
    """Module for parsing the Markdown specifications."""

    def __init__(
        self, md_data=None, table_prefix="table", entry_prefix="entry"
    ):
        if md_data is None:
            raise SpecParserError("No Markdown data found.")
        if not isinstance(md_data, list) or md_data == []:
            raise DataFormatError("Invalid Markdown data found.")
        self._md = md_data
        self._table = table_prefix
        self._entry = entry_prefix
        self._extracted = None
        self._extract_tables()
        self._organised = dict()
        self._organise_tables()

    def _extract_tables(self):
        """
        Extract tables from markdown data.

        Arguments
        ---------
            list of strings md_data -- markdown file contents

        Keyword Arguments
        -----------------
            string prefix -- prefix to use for the naming of the tables
                            (a number will be appended to the prefix
                            to generate the final name, e.g. table10)

        Returns
        -------
            dict of tables (every table is a list of list of strings)

        """
        block = False
        tbl_ctr = 0
        res = dict()
        table = []
        for line in self._md:
            line2 = line.strip().replace("`", "")
            if line2.startswith("|"):
                if not block:
                    block = True
                table.append(line2.split("|")[1:-1])
            else:
                if block:
                    block = False
                    res[self._table + str(tbl_ctr)] = table
                    tbl_ctr += 1
                    table = []
                else:
                    continue
        if table != []:
            res[self._table + str(tbl_ctr)] = table
        self._extracted = res

    def _organise_tables(self):
        """
        Extract table structure.

        Arguments
        ---------
            dict of tables tbl_data -- tables as extracted with extract_tables

        Keyword Arguments
        -----------------
            string prefix -- prefix for the name of every entry in the table

        Returns
        -------
            dict of tables (every table is a list of list of strings)


        """
        for key in self._extracted:
            tmp = dict()
            table = self._extracted[key]
            for pos, row in enumerate(table[2:]):
                tmp[self._entry + str(pos)] = dict(zip(table[0], row))
            self._organised[key] = tmp

    def get(self):
        """Return complete parsed spec."""
        return self._organised

    def is_valid(self):
        """Check if the parsing was successful."""
        return self._extracted is not None and self._organised != dict()
