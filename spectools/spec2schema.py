#! /usr/bin/env python3
"""WIP: Module for converting parsed spec to validator schema."""
import json
from spectools.specparser import SpecParser

# import conf.config as cc
INFILE = "docs/spec-metrics2.md"
JSONFILE = "out/tables.json"


def char2bool(req):
    """Convert characters like y/n into a boolean value."""
    if not isinstance(req, str):
        return None
    if len(req) != 1:
        return None
    if req in "yYtT":
        return True
    if req in "nNfF":
        return False
    return None


def cols2dict(table, keycol, valcol):
    """Extract values of two columns from table and return them as dict."""
    if not isinstance(table, dict):
        return None
    if not isinstance(keycol, str):
        return None
    if not isinstance(valcol, str):
        return None
    return {table[i][keycol]: table[i][valcol] for i in table}


def col2list(table, col):
    """Extract values of a column as list."""
    if not isinstance(table, dict):
        return None
    if not isinstance(col, str):
        return None
    return [table[i][col] for i in table if col in table[i]]


def entry2var(entry, varkey="Report metric"):
    """Extract metric name as the new key."""
    if not isinstance(entry, dict):
        return None
    if not isinstance(varkey, str):
        return None
    if varkey not in entry:
        return None
    varname = entry[varkey]
    del entry[varkey]
    return (varname, entry)


def constr2dict(name, constraint):
    """Split constraint string into parts and return as a dict."""
    if constraint.strip().startswith("regex"):
        return {"regex": constraint[constraint.find("=") + 1 :]}

    res = dict()
    parts = constraint.split(",")
    for part in parts:
        tmp = part.split("=")
        if name.startswith("INT"):
            res[tmp[0].strip()] = int(tmp[1].strip())
        elif name.startswith("FLOAT"):
            res[tmp[0].strip()] = float(tmp[1].strip())
        elif name.startswith("STR") or name.startswith("SAMPL"):
            res[tmp[0].strip()] = tmp[1].strip()
        elif name.startswith("MEM"):
            res[tmp[0].strip()] = int(tmp[1].strip())
        else:
            print("Constraint was not recognized ...")
    return res


def resolve_constraints(entry, key="Constraint", mapping=None):
    """Replace constraint code with valid rules.

    Arguments
    ---------
        entry -- dict containing the data for one variable
        key -- name of the constraint keyword in the entry
        mapping -- constraint code to rule set mapping

    Returns
    -------
        modified entry

    """
    if not isinstance(entry, dict):
        return None
    if not isinstance(key, str) or key not in entry:
        return None
    if not isinstance(mapping, dict):
        return None
    constraint = entry[key]
    if "Range" not in constraint:
        val = mapping[constraint]
        entry.update(val)
    else:
        tmp = constraint.split("[")[1].split("]")[0].split(",")
        entry["min"] = int(tmp[0])
        entry["max"] = int(tmp[1])
    del entry[key]
    return entry


def spec2json(infile=INFILE, outfile=JSONFILE):
    """Parse spec and save parsed data as JSON."""
    with open(infile, "r") as fil:
        data = fil.readlines()
    spp = SpecParser(data)
    tables = spp.get()
    with open(outfile, "w") as fil:
        json.dump(tables, fil)
